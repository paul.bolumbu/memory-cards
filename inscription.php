<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="formulaire.css">
</head>

<body>

<?php

require_once 'databases.php';

if (isset($_POST['valid_inscr'])) { // SI les cases sont vide cette ligne va permettre ou pas d'affirmer si c'est ok ou pas
    if (
        !empty($_POST['nom']) &&
        !empty($_POST['prenom']) &&
        !empty($_POST['email_inscr']) &&
        !empty($_POST['cmail_inscr']) &&
        !empty($_POST['mp_inscr']) &&
        !empty($_POST['mp_conf']) &&
        !empty($_POST['valid_inscr']) &&
        !empty($_POST['pseudo'])
    ) {


        $nom = htmlspecialchars($_POST['nom']);
        $prenom = htmlspecialchars($_POST['prenom']);
        $email = htmlspecialchars($_POST['email_inscr']); //htmlspecialchars permet d'éviter tous les caractères HTML pour éviter les injections de code
        $emailC = htmlspecialchars($_POST['cmail_inscr']);
        $user_pseudo = htmlspecialchars($_POST['pseudo']);
        $mp = password_hash($_POST['mp_inscr'], PASSWORD_DEFAULT); //méthode pour hashé le MDP
        $mpC = password_hash($_POST['mp_conf'], PASSWORD_DEFAULT);
        $PasDerreur = 'Informations correctes !';



        $pseudolength = strlen($user_pseudo);
        if ($pseudolength <= 255) // Vérifie si le nombre caractère dépasse pas les 255
        {
            if ($email == $emailC) // Vérifie si le email de confirmation correspond au email indiquer pour l'inscription
            {
                if (filter_var($email, FILTER_VALIDATE_EMAIL))  // Permet de voir si le 1er email rentré correspond
                {
                    $reqmail = $db->prepare("SELECT * FROM utilisateur WHERE email = ?");
                    $reqmail->execute(array($email));
                    $mailexist = $reqmail->rowCount();
                    if ($mailexist == 0) {
                        if ($_POST['mp_inscr'] == $_POST['mp_conf']) {
                            $insertmembre = $db->prepare("INSERT INTO utilisateur(pseudo, email, password) VALUES(?, ?, ?)");
                            $insertmembre->execute(array($user_pseudo, $email, $mp)); // pour exécuter la fonction
                            $erreur = "Félicitations ! Tu n'as pas gagner la coupe du monde mais ... Ton compte a été créé ! <a href=\"connexion.php\">Me connecter</a>";
                            //$$_SESSION['comptecree'] = "Félicitations ! Tu n'as pas gagner la coupe du monde mais ... Ton compte a été créé !";

                            header('Location: connexion.php'); // si l'utilisateur est créé il va etre rediriger là
                        } else {
                            $erreur = "Vos mot de passe ne correspondent pas ! Tu es fatigué ?";
                            echo "1";
                        }
                    } else //Empêche une adresse email identique d'être réutiliser
                    {
                        $erreur = "Adresse email déjà utilisée !";
                        echo "2";
                    }
                }
            } else {
                $erreur = "Vos adresses e-mail ne correspondent pas ! Oh !";
                echo "3";
            }
        } else {
            $erreur = "Eh, mon ami ! Votre nom d'utilisateur ne doit dépasser 255 caractères, désolé !";
            echo "4";
        }
    } else {
        $erreur = 'Tous les champs doivent être remplis';
        echo "5";

        // echo "Paul te dit que non, tout n'est pas ok !"; // si on tente de s'inscrire si les cases sont vide
    }
}

// $hash = '$2y$10$NRQaWuXgWBAJ1PgmOrRViODp/9kqx1.tdqIGUQUUsKE3/k2vpyE4q';

// if (password_verify('lisaetpaul', $hash)) {
//     echo 'Le mot de passe est valide !';
// } else {
//     echo 'Le mot de passe est invalide.';
// }

?>



<?php

// C'est un tableau qui va aider à la traduction. 0 correspond au français et 1 à l'anglais.
// Des variables sont déclarés et dedans , il y a des mots dans les deux langues.

$langue = 0;
if (isset($_GET['lang']))
    $langue = 1;
$titre = array('Formulaire d\'inscription', 'Registration Form');
$annonce = array('Veuillez remplir tous les champs du formulaire', 'Please fill in all fields of the form');
$civilite = array('Civilité', 'Civility');
$pseudo = array('Votre pseudo', 'Your pseudo');
$madame = array('Madame', 'Mrs');
$monsieur = array('Monsieur', 'Mr.');
$docteur = array('Docteur', 'Doctor');
$nom = array('Votre nom', 'Your name');
$prenom = array('Votre prénom', 'Your first name');
$naissance = array('Naissance, ex : 19/04/1996', 'birth, ex : 19/04/1996');
$email = array('Votre mail', 'Your e-mail');
$emailC = array('Confirmer le mail', 'Confirm the e-mail');
$mp = array('Votre mot de passe : entre 5 et 10 caractères', 'Your password : between 5 and 10 characters');
$mpC = array('Confirmer le mot de passe :', 'Confirm password :');
$btn = array('Valider', 'Validate');




?>


<div class="div_saut_ligne">
</div>

<div style="float:left;width:10%;height:40px;"></div>
<div style="float:left;width:80%;height:40px;text-align:center;">
    <div id="GTitre">
        <h1><?php echo $titre[$langue]; ?></h1>
    </div>
</div>
<div style="float:left;width:10%;height:40px;"></div>

<div class="div_saut_ligne" style="height:60px">
</div>

<div style="width:100%;height:auto;text-align:center;">

    <div style="width:800px;display:inline-block;" id="conteneur">
        <div id="centre">
            <div id="message">
                <?php echo $annonce[$langue]; ?>
                <div style="float:right;">
                    <a href="." target="_self"><img src="images/drapeau-francais.png" class="drapeau" /></a>
                    <a href="index.php?lang=1" target="_self"><img src="images/drapeau-anglais.png" class="drapeau" /></a>
                </div>
            </div>
            <form id="inscription" name="inscription" method="post" action="index.php">
                <div class="div_input_form">
                    <select id="civilite" name="civilite" onChange="Javascript:if(this.value>0){ b_civilite=true; } else{ b_civilite=false; }">
                        <option value="<?php echo $civilite[$langue]; ?>"><?php echo $civilite[$langue]; ?></option>
                        <option value="<?php echo $madame[$langue]; ?>"><?php echo $madame[$langue]; ?></option>
                        <option value="<?php echo $monsieur[$langue]; ?>"><?php echo $monsieur[$langue]; ?></option>
                        <option value="<?php echo $docteur[$langue]; ?>"><?php echo $docteur[$langue]; ?></option>
                    </select>
                </div>
                <div class="div_input_form">
                    <input type="text" name="pseudo" id="pseudo" maxlength="50" class="input_form" value='<?php echo $pseudo[$langue]; ?>' onClick="saisie('<?php echo $pseudo[$langue]; ?>', this.id)" onMouseOut="retablir('<?php echo $pseudo[$langue]; ?>',this.id)" onblur="mev('<?php echo $pseudo[$langue]; ?>',this.id)" />
                </div>

                <div class="div_input_form">
                    <input type="text" name="nom" id="nom" maxlength="50" class="input_form" value='<?php echo $nom[$langue]; ?>' onClick='saisie(' <?php echo $nom[$langue]; ?>', this.id)' onMouseOut='retablir(' <?php echo $nom[$langue]; ?>', this.id)' onblur='mev(' <?php echo $nom[$langue]; ?>', this.id)' />
                </div>
                <div class="div_input_form">
                    <input type="text" name="prenom" id="prenom" maxlength="50" class="input_form" value='<?php echo $prenom[$langue]; ?>' onClick="saisie('<?php echo $prenom[$langue]; ?>', this.id)" onMouseOut="retablir('<?php echo $prenom[$langue]; ?>',this.id)" onblur="mev('<?php echo $prenom[$langue]; ?>',this.id)" />
                </div>
                <div class="div_input_form">
                    <input type="text" name="date_n" id="date_n" maxlength="50" class="input_form" value='<?php echo $naissance[$langue]; ?>' onClick="saisie('<?php echo $naissance[$langue]; ?>)" onMouseOut="retablir('<?php echo $naissance[$langue]; ?>',this.id)" onblur="mev('<?php echo $naissance[$langue]; ?>',this.id)" />
                </div>
                <div class="div_input_form">
                    <input type="text" name="email_inscr" id="email_inscr" maxlength="150" class="input_form" value='<?php echo $email[$langue]; ?>' onClick="saisie('<?php echo $email[$langue]; ?>)" onMouseOut="retablir('<?php echo $email[$langue]; ?>', this.id)" onblur="mev('<?php echo $email[$langue]; ?>',this.id)" />
                </div>
                <div class="div_input_form">
                    <input type="text" name="cmail_inscr" id="cmail_inscr" maxlength="150" class="input_form" value='<?php echo $emailC[$langue]; ?>' onClick="saisie('<?php echo $emailC[$langue]; ?>',this.id)" onMouseOut="retablir('<?php echo $emailC[$langue]; ?>',this.id)" onblur="mev('<?php echo $emailC[$langue]; ?>',this.id)" />
                </div>
                <div class="div_input_form">
                    <?php echo $mp[$langue]; ?><br />
                    <input type="password" name="mp_inscr" id="mp_inscr" maxlength="10" class="input_form" value='<?php echo $mp[$langue]; ?>' onClick="saisie('<?php echo $mp[$langue]; ?>',this.id)" onMouseOut="retablir('<?php echo $mp[$langue]; ?>',this.id)" onblur="mev('<?php echo $mp[$langue]; ?>',this.id)" />
                </div>
                <div class="div_input_form">
                    <?php echo $mpC[$langue]; ?><br />
                    <input type="password" name="mp_conf" id="mp_conf" maxlength="10" class="input_form" value="<?php echo $mpC[$langue]; ?>" onClick="saisie('<?php echo $mpC[$langue]; ?>',this.id)" onMouseOut="retablir('Confirmer MP',this.id)" onblur="mev('Confirmer MP',this.id)" />
                </div>
                <div class="div_input_form">
                    <input type="submit" name="valid_inscr" id="valid_inscr" class="input_form" value="<?php echo $btn[$langue]; ?>" onclick="envoyer()" />

                </div>
            </form>
        </div>
    </div>

</div>
<?php
if (isset($erreur)) {
    echo "<div class='error'>$erreur</div>";
} else if (isset($PasDerreur)) {
    echo "<div class='not_error'>$PasDerreur</div>";
}
?>

<div class="div_saut_ligne" style="height:150px;">
</div>