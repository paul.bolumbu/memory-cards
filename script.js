// Partie pour retourner la carte
let cards = document.querySelectorAll('.card');

[...cards].forEach((card) => {
  card.addEventListener('click', function () {
    card.classList.toggle('is-flipped');
  });
});



// Pour faire changer le dark mode

const switchThemeBtn = document.querySelector('.checkbox-wrapper-54')

switchThemeBtn.addEventListener('change', (e) => {
   if (e.target.checked === false ) {

    document.documentElement.style.setProperty('--ecriture', 'var(--couleur-front)');
    document.documentElement.style.setProperty('--background', 'var(--couleur-back)');

  } else {
    document.documentElement.style.setProperty('--ecriture', 'var(--couleur-back)');
    document.documentElement.style.setProperty('--background', 'var(--couleur-front)');

  }

})



// Test pour le bouton de la carte. Pour empêcher la carte de se retourner 
document.querySelector('.screen1').addEventListener('click', function(event) {
  event.stopPropagation();
  console.log('On a cliqué sur le bouton test')
})


// Test numéro 2 pour faire la même chose avec l'autre carte
document.querySelector('.screen2').addEventListener('click', function(event) {
  event.stopPropagation();
  console.log('On a cliqué sur le bouton test')
})